#include <assert.h>
#include <cctype>
#include <iostream>

#include "Game.h"
#include "Chess.h"
// GOOD
#include "Prompts.h"
#include "Interface.h"

///////////////
//// Board ////
///////////////

Board::~Board() {
    // Delete all pointer-based resources
    for (unsigned int i = 0; i < m_width * m_height; i++)
        delete m_pieces[i];
    for (size_t i = 0; i < m_registeredFactories.size(); i++)
        delete m_registeredFactories[i];
}

// Get the Piece at a specific Position, or nullptr if there is no
// Piece there or if out of bounds.
Piece* Board::getPiece(Position position) const {
    if (validPosition(position))
        return m_pieces[index(position)];
    else {
        Prompts::outOfBounds();
        return nullptr;
    }
}

// Create a piece on the board using the factory.
// Returns true if the piece was successfully placed on the board
bool Board::initPiece(int id, Player owner, Position position) {
    Piece* piece = newPiece(id, owner);
    if (!piece) return false;

    // Fail if the position is out of bounds
    if (!validPosition(position)) {
        Prompts::outOfBounds();
        return false;
    }
    // Fail if the position is occupied
    if (getPiece(position)) {
        Prompts::blocked();
        return false;
    }
    m_pieces[index(position)] = piece;
    return true;
}

// Add a factory to the Board to enable producing
// a certain type of piece
bool Board::addFactory(AbstractPieceFactory* pGen) {
    // Temporary piece to get the ID
    Piece* p = pGen->newPiece(WHITE);
    int id = p->id();
    delete p;

    PieceGenMap::iterator it = m_registeredFactories.find(id);
    if (it == m_registeredFactories.end()) { // not found
        m_registeredFactories[id] = pGen;
        return true;
    } else {
        std::cout << "Id " << id << " already has a generator\n";
        return false;
    }
}

// Search the factories to find a factory that can translate `id' to
// a Piece, and use it to create the Piece. Returns nullptr if not found.
Piece* Board::newPiece(int id, Player owner) {
    PieceGenMap::iterator it = m_registeredFactories.find(id);
    if (it == m_registeredFactories.end()) { // not found
        std::cout << "Id " << id << " has no generator\n";
        return nullptr;
    } else {
        return it->second->newPiece(owner);
    }
}

// Gives the current position that a piece is on.
Position Board::getPosition(Piece* piece) const {
    unsigned int x;
    unsigned int y;
    int index;

    // Find the entry of the Piece pointer.
    for (unsigned int k = 0; k < m_pieces.size(); k++) {
        // If the piece is found, return its index in m_pieces.
        if (m_pieces.at(k) == piece) {
            index = k;
            break;
        }
    }

    // Use the Division Algorithm Theorem to undo index(Position)
    x = index % m_width;
    y = index / m_width;
    return Position(x, y);
}

// Check if there are piece in between a move. Also check that the final
// position isn't occupied by a friendly piece.
int Board::checkInBetween(Position& start, Position& end) const {
    // Correcting if a start or end coordinate is greater or less than the other
    int dx = 1;
    int dy = 1;
    if (start.x > end.x) dx = -1;
    if (start.y > end.y) dy = -1;
    int x = start.x + dx;
    int y = start.y + dy;

    if (start.x == end.x) {
        // Rook moving forward/backward, Pawn moving forward.
        while (y != static_cast<int> (end.y)) {
            int k = index(Position(start.x, y));
            if (m_pieces.at(k) != nullptr) return MOVE_ERROR_BLOCKED;
            y += dy;
        }
    } else if (start.y == end.y) {
        // Rook moving left/right.
        while (x != static_cast<int> (end.x)) {
            int k = index(Position(x, start.y));
            if (m_pieces.at(k) != nullptr) return MOVE_ERROR_BLOCKED;
            x += dx;
        }
    } else {
        // Bishop move, Pawn moving diagonally.
        // Note that if x == end.x, y == end.y also.
        while (x != static_cast<int> (end.x)) {
            int k = index(Position(x, y));
            if (m_pieces.at(k) != nullptr) return MOVE_ERROR_BLOCKED;
            x += dx;
            y += dy;
        }
    }
    // Check that the final position isn't occupied by a friendly piece.
    Piece* startPiece = getPiece(start);
    Piece* endPiece = getPiece(end);
    if (endPiece != nullptr && endPiece->owner() == startPiece->owner()) {
        return MOVE_ERROR_BLOCKED;
    } else return SUCCESS;
}

// The main gameplay loop. Ideally, you should be able to implement
// all of the gameplay loop logic here in the Board class rather than
// overriding this method in the specialized Game-specific class
void Board::run() {
    while (!gameOver()) { // As long as gameOver == 0, i.e. is false.
        doTurn();
    }
    // Display the endgame message.
    GameInterface::displayEndMsg(gameOver(), playerTurn(), m_turn);
}

// Default implementation of the validMove method for a piece.
// Here, a piece's move is valid if it's not out of bounds and if the start
// and end positions are different. (Hence a mystery piece that implements no
// further functionality can move anywhere on the board instantly.)
int Piece::validMove(Position start, Position end, const Board& board) const {
    // Check that the start and end positions are in the bounds of the board.
    if (!(board.validPosition(start)) || !(board.validPosition(end))) {
        return MOVE_ERROR_OUT_OF_BOUNDS;
    } else if (start.x == end.x && start.y == end.y) {
        // Check that start and end positions are actually different.
        return MOVE_ERROR_ILLEGAL;
    } else {
        return SUCCESS; // SUCCESS == 1
    }
}
