#undef FOR_RELEASE

#include <tuple>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cmath>

#include "Chess.h"
#include "FileIO.h"
#include "Game.h"
#include "Interface.h"
#include "Prompts.h"


// PIECE MOVE VALIDATION

// Validation for moving the pawn.
int Pawn::validMove(Position start, Position end, const Board& board) const {
    // Make sure that the pawn movement follows generic rules
    // (not out of bounds and start != end).
    int checkGen = Piece::validMove(start, end, board);
    if (checkGen != 1) return checkGen;

    // The type of pawn move
    // 0 = non-capture (move forwards)
    // 1 = catpure (move diagonally)
    // -1 = flat-out illegal.
    int moveType = -1;
    // The difference between end and start position.
    // For white pawn, this difference is 1 (as pawn moves forwards).
    // For black pawn, this difference is -1 (as pawn moves "backwards" w.r.t. board).
    int dy = 1;
    if (m_owner == BLACK) dy = -1;

    // Signed versions of the start and end y coords (to avoid type/undeflow errors).
    int startY = start.y;
    int endY = end.y;

    // If the pawn moves forwards.
    if (start.x == end.x) {
        // If pawn moves by 2 squares, make sure it starts on its starting row.
        if (((endY - startY) == (2 * dy)) && (startY == (-5 * dy + 7) / 2)) {
            // For white, if endY - startY == 2 and startY == 1
            // For black, if endY - startY == -2 and startY == 6
            // Note: (-5(dy) + 7)/2 equals 1 if dy == 1, or 6 if dy == -1.
            moveType = 0;
        } else if (endY - startY == dy) { // If pawn moves by 1 square.
            moveType = 0;
        }
    } else if (abs(end.x - start.x) == 1) { // If pawn moves diagonally.
        if (endY - startY == dy) { // Make sure pawn only moves by 1 vertically.
            moveType = 1;
        }
    }

    if (moveType == 0) { // Regular pawn movement.
        if (board.getPiece(end) != nullptr) {
            // If the ending piece is occupied, whether by a friendly or enemy piece.
            return MOVE_ERROR_BLOCKED;
        } else if (board.checkInBetween(start, end) != SUCCESS) {
            // Also check in between (only really useful for move by 2 squares).
            return MOVE_ERROR_BLOCKED;
        } else {
            // We can move forwards, so try move.
            return SUCCESS;
        }
    } else if (moveType == 1) { // Pawn captures enemy piece.
        Piece* endPiece = board.getPiece(end);
        // This move is only legal iff there's an enemy piece on the final position
        if (endPiece != nullptr) { // If final piece exists.
            if (endPiece->owner() != m_owner) { // If other piece is an enemy.
                return SUCCESS;
            } else { // If other piece is friendly.
                return MOVE_ERROR_ILLEGAL;
            }
        } else {
            return MOVE_ERROR_ILLEGAL;
        }
    } else { // If the move is illegal.
        return MOVE_ERROR_ILLEGAL;
    }
}

// Validation for moving the bishop.
int Bishop::validMove(Position start, Position end, const Board& board) const {
    // Make sure that the pawn movement follows generic rules
    // (not out of bounds and start != end).
    int checkGen = Piece::validMove(start, end, board);
    if (checkGen != 1) return checkGen;

    // Signed versions of the start and end positions (to avoid type/undeflow errors).
    int startX = start.x;
    int startY = start.y;
    int endX = end.x;
    int endY = end.y;

    // For the bishop's diagonal moves, the vertical change in distance must always
    // be the same as the horizontal change in distance.
    if (!(abs(endX - startX) == abs(endY - startY))) { // If move is not diagonal.
        return MOVE_ERROR_ILLEGAL;
    } else if (board.checkInBetween(start, end) != SUCCESS) { // If blockage occurs.
        return MOVE_ERROR_BLOCKED;
    } else {
        return SUCCESS; //board.determineCheck(m_owner, start, end, this);
    }
}

// Validation for moving the knight.
int Knight::validMove(Position start, Position end, const Board& board) const {
    // Make sure that the pawn movement follows generic rules
    // (not out of bounds and start != end).
    int checkGen = Piece::validMove(start, end, board);
    if (checkGen != 1) return checkGen;

    // Signed versions of the start and end positions (to avoid type/undeflow errors).
    int startX = start.x;
    int startY = start.y;
    int endX = end.x;
    int endY = end.y;

    // For the knight's L-shaped move, either the horizontal distance is 2 and the
    // vertical distance 1 (a sideways L) or the horizontal distance is 1 and the
    // vertical distance 2 (a vertical L). Note that we don't check for blockage
    // except for the case of "friendly piece in end position."
    if (!((abs(endX - startX) == 2 && abs(endY - startY) == 1)
            || ((abs(endX - startX) == 1) && abs(endY - startY) == 2))) {
        return MOVE_ERROR_ILLEGAL;
    } else {
        // Check that no friendly piece is on the final position.
        if (board.getPiece(end) != nullptr && board.getPiece(end)->owner() == m_owner) {
            return MOVE_ERROR_BLOCKED;
        } else {
            return SUCCESS;
        }
    }
}

// Validation for moving the rook.
int Rook::validMove(Position start, Position end, const Board& board) const {
    // Make sure that the pawn movement follows generic rules
    // (not out of bounds and start != end).
    int checkGen = Piece::validMove(start, end, board);
    if (checkGen != 1) return checkGen;

    // The rook's move is legal if either the horizontal or vertical start and
    // end positions are the same. (Previous checks prevent both from being the same.)
    if (!(start.x == end.x || start.y == end.y)) {
        return MOVE_ERROR_ILLEGAL;
    } else if (board.checkInBetween(start, end) != SUCCESS) { // If blockage occurs.
        return MOVE_ERROR_BLOCKED;
    } else {
        return SUCCESS;
    }
}

// Validation for moving the quen.
int Queen::validMove(Position start, Position end, const Board& board) const {
    // Make sure that the pawn movement follows generic rules
    // (not out of bounds and start != end).
    int checkGen = Piece::validMove(start, end, board);
    if (checkGen != 1) return checkGen;

    // Signed versions of the start and end positions (to avoid type/undeflow errors).
    int startX = start.x;
    int startY = start.y;
    int endX = end.x;
    int endY = end.y;
    bool isMoveIllegal  = false;

    // The queen's move is legal if if either the rook's or bishop's rules are met.
    if (abs(endX - startX) == abs(endY - startY) ||
            (start.x == end.x || start.y == end.y)) {
        isMoveIllegal = true;
    }

    if (!isMoveIllegal) { // If neither ruleset is met.
        return MOVE_ERROR_ILLEGAL;
    } else if (board.checkInBetween(start, end) != SUCCESS) { // If queen is blocked.
        return MOVE_ERROR_BLOCKED;
    } else { // If all tests pass.
        return SUCCESS;
    }
}

// Validation for moving the king.
int King::validMove(Position start, Position end, const Board& board) const {
    // Make sure that the pawn movement follows generic rules
    // (not out of bounds and start != end).
    int checkGen = Piece::validMove(start, end, board);
    if (checkGen != 1) return checkGen;

    // Signed versions of the start and end positions (to avoid type/undeflow errors).
    int startX = start.x;
    int startY = start.y;
    int endX = end.x;
    int endY = end.y;

    // The king can only move a maximum of 1 square horizontally
    // and 1 square vertically. Anything more than that is illegal.
    // The only exception is during castling, which is handled separately.
    int castleCode = validCastle(start, end, board);
    if (castleCode != 0) {
        // An attempt was made at castling; display the appropriate code.
        return castleCode;
    } else if (!(abs(endX - startX) <= 1 && abs(endY - startY) <= 1)) {
        return MOVE_ERROR_ILLEGAL;
    } else if (board.getPiece(end) != nullptr &&
               board.getPiece(end)->owner() == m_owner) {
        // If there's a blockage by a friendly piece.
        return MOVE_ERROR_BLOCKED;
    } else {
        return SUCCESS;
    }
}

int King::validCastle(Position start, Position end, const Board& board) const {
    // Signed versions of the start and end positions (to avoid type/undeflow errors).
    int startX = start.x;
    int startY = start.y;
    int endX = end.x;
    int endY = end.y;

    if (!(abs(endX - startX) == 2 && endY - startY == 0)) {
        // Player wasn't trying to castle in the first place.
        return 0; // Return the default value.
    } else if ((m_owner == WHITE && !(startX == 4 && startY == 0)) ||
               (m_owner == BLACK && !(startX == 4 && startY == 7))) {
        // The king is not on the right starting spot.
        // (Note: This provides an extra check after starting a loaded game.)
        return MOVE_ERROR_CANT_CASTLE;
    } else if (m_hasMoved) {
        // We cannot castle with a king that has already moved.
        return MOVE_ERROR_CANT_CASTLE;
    }

    // Get the rook.
    Position rookPos; // The position of the rook itself.
    Position rookAdj; // The position of the square adjacent to the rook.
    if (endX - startX == 2) { // Castling kingside.
        if (m_owner == WHITE) rookPos = Position(7, 0);
        else rookPos = Position(7, 7);
        rookAdj = Position(6, rookPos.y);
    } else { // Castling queenside.
        if (m_owner == WHITE) rookPos = Position(0, 0);
        else rookPos = Position(0, 7);
        rookAdj = Position(1, rookPos.y);
    }
    Piece* rook = board.getPiece(rookPos);

    // Check if the rook exists, is actually a rook, is owned by the current player,
    // and itself hasn't moved.
    if (!(rook != nullptr && rook->id() == ROOK_ENUM &&
            rook->owner() == m_owner && !(rook->hasMoved()))) {
        // If it fails any one of these conditions.
        return MOVE_ERROR_CANT_CASTLE;
    } else if (board.checkInBetween(start, rookAdj) == MOVE_ERROR_BLOCKED) {
        // There is a piece between the rook and king, which is illegal.
        return MOVE_ERROR_BLOCKED;
    } else {
        // Castling is possible.
        return MOVE_CASTLE;
    }
}

// Do a turn, which can include saving, board displaying, or moving a piece.
void ChessGame::doTurn() {
    // Whether we failed at our task during turn and must repeat it (FALSE) or
    // if our turn does not result in errors and can end (TRUE).
    bool isTurnSuccess = false;
    // Display the board at the beginning of every turn.
    m_boardDisplay.displayBoard();
    do {
        // Get input from the player on what to do this turn.
        std::string input = GameInterface::getIngameInput(playerTurn(), m_turn);

        if (input == "q") { // Quit game.
            endGame(GAME_QUIT);
            return;
        } else if (input == "board") { // Toggle board display.
            m_boardDisplay.toggleDisplay();
            m_boardDisplay.displayBoard();
        } else if (input == "save") { // Save game.
            try {
                FileHandler fh(false);
                fh.writeFile(m_width, m_turn, m_pieces);
            } catch (std::runtime_error& e) {
                // If we cannot save the game.
                Prompts::saveFailure();
            }
        } else if (input == "forfeit") { // Forfeit game.
            endGame(GAME_FORFEIT);
            return;
        } else { // Make a move.
            std::pair<Position, Position> posPair = GameInterface::parsePosition(input);
            int moveStatus = makeMove(std::get<0>(posPair), std::get<1>(posPair));
            // Take action whether the game ends or if a regular turn is made.
            if (moveStatus == MOVE_CHECKMATE || moveStatus == MOVE_STALEMATE) {
                // Display the final status of the board.
                m_boardDisplay.displayBoard();
                // Return with the end status of the game.
                endGame(static_cast<status>(moveStatus));
                return;
            } else if (moveStatus > 0) { // We have succeeded moving the piece.
                isTurnSuccess = true;
            }
            GameInterface::displayMoveMsg(moveStatus, playerTurn());
        }
    } while (!isTurnSuccess);
    // Increment the turn counter.
    ++m_turn;
}
// Make a move on the board. Return an int, with < 0 being failure
int ChessGame::makeMove(Position start, Position end) {
    // Implement chess-specific move logic here

    // We call Board's makeMove to handle any general move logic
    // Feel free to use this or change it as you see fit
    // int retCode = Board::makeMove(start, end);

    // Get the current player based on the turn number.
    // Remember, white starts on Turn 1, so odd turns are
    // white's and even turns are black's.
    Player myPlayer = static_cast<Player>(playerTurn());

    // Attempt to get a piece at this position.
    // Return an error if there is no piece or if the piece is owned by the enemy.
    Piece* piece = getPiece(start);
    if (piece == nullptr || piece->owner() != myPlayer) {
        return MOVE_ERROR_NO_PIECE;
    }

    // Check that the piece's move is valid.
    int moveValidity = piece->validMove(start, end, *this);
    if (moveValidity < 0) return moveValidity; // The move was a failure.

    // Check if our move would put the current player's king in check.
    int successCode = SUCCESS;
    // Handle castling separately.
    if (moveValidity == MOVE_CASTLE) {
        successCode = makeCastle(myPlayer, start, end, piece);
    } else { // For regular moves.
        successCode = determineCheck(myPlayer, start, end, piece);
        // If we are successful at moving the piece.
        if (successCode >= 0) {
            // Temporary pointer that represents the end position.
            Piece* tempPiece = getPiece(end);
            // The piece we're moving now occupies the ending piece.
            m_pieces.at(index(end)) = piece;
            // The starting position is cleared.
            m_pieces.at(index(start)) = nullptr;

            // If possible, capture an enemy piece.
            if (tempPiece != nullptr) {
                delete tempPiece;
                successCode = MOVE_CAPTURE;
            }
            // Promote the piece if it's a pawn on a end space.
            if ((getPiece(end)->id() == PAWN_ENUM) &&
                    ((myPlayer == WHITE && end.y == 7) ||
                     (myPlayer == BLACK && end.y == 0))) {
                promotePawn(end, myPlayer);
            }
            // Change the piece's hasMoved status if it's the first time it has moved.
            if (!getPiece(end)->hasMoved()) getPiece(end)->setHasMoved(true);
            // Check if the enemy is checkmated, stalemated, or checked.
            int mateCode = determineMate();
            //If the enemy is checkmated, stalemated, or checked, notify the game.
            if (mateCode == MOVE_CHECKMATE || mateCode == MOVE_STALEMATE ||
                    mateCode == MOVE_CHECK) {
                successCode = mateCode;
            }
            // Determine if you have stalemated yourself.
            // Note that this only happens if you haven't ended the game already.
            if (successCode != MOVE_CHECKMATE || successCode != MOVE_STALEMATE) {
                int stalemateCode = determineStalemate(myPlayer);
                if (stalemateCode == MOVE_STALEMATE) successCode = stalemateCode;
            }
        }
    }
    // Return the success type.
    return successCode;
}

// Attempt to complete a castling move.
int ChessGame::makeCastle(Player myPlayer, Position start, Position end, Piece* king) {
    // We cannot castle if we are currently in check.
    if (m_checkStatus.at(myPlayer)) return MOVE_ERROR_CANT_CASTLE;

    // Signed versions of the x coordinates (to avoid type/undeflow errors).
    int startX = start.x;
    int endX = end.x;
    // The y coordinate, which must stay the same throughout.
    int yCoord = start.y;

    // The rook and its position.
    Position rookPos;
    Piece* rook;
    if (startX < endX) { // Castling kingside.
        rookPos = Position(7, yCoord);
    } else { // Castling queenside.
        rookPos = Position(0, yCoord);
    }
    rook = getPiece(rookPos);

    int dx = 1; // Default is casling kingside.
    if (startX > endX) { // Change if castling queenside.
        dx = -1;
    }

    // Check that the squares between the starting position and the ending position
    // (including the ending position itself) are not under check.
    if (isUnderAttack(Position(startX + dx, yCoord), myPlayer, true).size() > 0) {
        return MOVE_ERROR_CANT_CASTLE;
    } else if (isUnderAttack(Position(startX + 2 * dx, yCoord), myPlayer, true).size() > 0) {
        return MOVE_ERROR_CANT_EXPOSE_CHECK;
        // No need for MOVE_ERROR_MUST_HANDLE_CHECK, for we know by now that the
        // king is not in check.
    }

    int successCode = MOVE_CASTLE;
    // We have passed the required tests, so we shall castle.
    // We first move the king.
    m_pieces.at(index(end)) = king;
    m_pieces.at(index(start)) = nullptr;  // The start king position is cleared.
    // We then move the rook.
    m_pieces.at(index(Position(endX - dx, yCoord))) = rook;
    m_pieces.at(index(rookPos)) = nullptr; // The start rook position is cleared.
    // The rook and king have now moved.
    rook->setHasMoved(true);
    king->setHasMoved(true);
    // Determine if we have checkmated, stalemated, or castled.
    // (We never move through check, so the king will always have a space to escape
    // to, so we never check if we have stalemated ourselves.)
    successCode = determineMate();
    // We have successfully castled.
    // (Note: We can never capture a piece while castling, so we never return
    // MOVE_CAPTURE.)
    return successCode;
}

// Promote the pawn to a queen.
void ChessGame::promotePawn(Position pawnPos, Player player) {
    // Delete the pawn on the space.
    delete getPiece(pawnPos);
    m_pieces.at(index(pawnPos)) = nullptr;
    // Create a queen in its place.
    initPiece(QUEEN_ENUM, player, pawnPos);
}

// Determine if a move by the player will result in the king being exposed to check.
int ChessGame::determineCheck(Player player, Position start, Position end, Piece* piece) {
    // The status of whether we put our king in check.
    int checkStatus = SUCCESS;

    // Attempt to move the pieces.
    // Temporary pointer that represents the end position.
    Piece* tempPiece = getPiece(end);
    // The piece we're moving now occupies the ending piece.
    m_pieces.at(index(end)) = piece;
    m_pieces.at(index(start)) = nullptr;  // The starting position is cleared.

    // Determine if our king, after the move attempt, is now in check.
    std::vector<Piece*> attackers = isUnderAttack(findKingPosition(player), player, true);
    if (attackers.size() > 0) { // The king is in check.
        if (m_checkStatus.at(player)) {
            // If player is already in check
            checkStatus = MOVE_ERROR_MUST_HANDLE_CHECK;
        } else {
            checkStatus = MOVE_ERROR_CANT_EXPOSE_CHECK;
        }
    }

    // This is just an attempt at a move, so put back the pieces to where they belong.
    m_pieces.at(index(end)) = tempPiece;
    m_pieces.at(index(start)) = piece;
    tempPiece = nullptr;
    // Return whether the player is in check or not.
    // If the player doesn't put themselves in check, it returns SUCCESS
    return checkStatus;
}

// Determine if there exists any  possible moves for a piece.
bool ChessGame::spreadCheck(Position start, Piece* piece) {

    // Signed versions of start.x and start.y
    int startX = start.x;
    int startY = start.y;

    /* We loop over a 5x5 Big Fat Cross centered on the piece.
     * We are able to do this because for all pieces, the range of
     * their valid moves are a subset of that cross.
     * This will work even if part of the cross is out of bounds or if
     * moving to a square is invalid for that piece.
     * Note that for rook, bishop, and queen, we only need to check
     * the squares immediately adjacent to them.
     */

    // Loop over the columns of the cross.
    for (int x = startX - 2; x < startX + 3; ++x) {
        // For the first and last columns of the cross, only check
        // three squares. Otherwise check five squares.
        int radius = 2;
        if (x == startX - 2 || x == startX + 2) radius = 1;
        // Loop over each square in the column.
        for (int y = startY - radius; y < startY + radius + 1; ++y) {
            // Skip over the position of the piece itself.
            if (x == startX && y == startY) continue;
            // Determine that moving to that square is valid and
            // that moving to it doesn't put the player's king in check.
            if ((piece->validMove(start, Position(x, y), *this) >= 0) &&
                    (determineCheck(piece->owner(), start, Position(x, y), piece) >= 0)) {
                // We have found a valid move for that piece.
                return true;
            }
        }
    }
    // We have not found a valid move for the piece.
    return false;
}

// Find the position of the king of myPlayer.
Position ChessGame::findKingPosition(Player myPlayer) const {
    // The king's position.
    Position kingPos;

    // Iterate over all pieces on the board to find my king.
    for (unsigned x = 0; x < m_width; ++x) {
        for (unsigned y = 0; y < m_height; ++y) {
            Piece* p = getPiece(Position(x, y));
            if (p != nullptr && p->id() == KING_ENUM && p->owner() == myPlayer) {
                kingPos = Position(x, y);
            }
        }
    }
    return kingPos;
}

// Determine if a position is able to be attacked by an enemy piece.
// Note: This does not check for if moving the enemy piece exposes the enemy King
// to check.
std::vector<Piece*> ChessGame::isUnderAttack(Position pos, Player myPlayer, bool countPawns) {
    // Determine the enemy player.
    // Remember, for white, 1 - 0 = 1 = black, and vice versa.
    Player enemyPlayer = static_cast<Player>(1 - myPlayer);

    // The vector of all of the pieces that can attack the position.
    std::vector<Piece*> killers;
    // Iterate over all the enemy pieces
    for (unsigned x = 0; x < m_width; ++x) {
        for (unsigned y = 0; y < m_height; ++y) {
            // Get the piece from our current position in the loop.
            Piece* p = getPiece(Position(x, y));
            // If we do have an enemy piece..
            if (p != nullptr && p->owner() == enemyPlayer) {
                // If the enemy piece can make a valid move against our position...
                if ((p->validMove(Position(x, y), pos, *this)) == SUCCESS) {
                    killers.push_back(p);
                } else if (countPawns && (p->id() == PAWN_ENUM)) {
                    // Also, if countPawns is true and the enemy piece is a pawn,
                    // bypass calling validMove.
                    int dy = 1;
                    if (p->owner() == BLACK) dy = -1;
                    if (abs(pos.x - x) == 1 &&
                            (static_cast<int>(pos.y - y) == dy)) {
                        // The pawn is able to reach Position(x,y).
                        killers.push_back(p);
                    }
                }
            }
        }
    }
    // If there are no pieces that can attack our position, return with a null pointer.
    // Otherwise, return the first checking piece found and the number of such pieces.
    return killers;
}

// Determine if piece that checks enemy King can be blocked by enemy piece.
bool ChessGame::canBlockCheck(Position start, Position end, Player myPlayer) {
    // Signed versions of the positions' unsigned integers.
    int startX = start.x;
    int startY = start.y;
    int endX = end.x;
    int endY = end.y;

    // Determine by how much we need to increment by.
    int dx = 1;
    int dy = 1;
    if (start.x == end.x) dx = 0;
    if (start.y == end.y) dy = 0;
    if (start.x > end.x) dx = -1;
    if (start.y > end.y) dy = -1;

    // The coordinates of the positions we iterate over.
    int x = startX;
    int y = startY;

    // While the position we are examining is not the victim piece's position.
    while (!(x == endX && y == endY)) {
        // If any of the spots between the start and target
        // can be filled by an enemy piece, then return true.
        bool countPawns = false;
        if (x == endX && y == endY) countPawns = true;
        std::vector<Piece*> attackers = isUnderAttack(Position(x, y), myPlayer, countPawns);
        if (attackers.size() > 0) {
            // If the attacker's move doesn't put their king in check,
            // then they can make a successful block.
            for (Piece* p : attackers) {
                if (determineCheck(static_cast<Player>(1 - myPlayer),
                                   getPosition(p), Position(x, y), p) >= 0) {
                    return true;
                }
            }
        }
        // For knights, we only check if we can capture the knight.
        // Checking for blockage will result in an infinite loop.
        if (getPiece(start)->id() == KNIGHT_ENUM) break;
        x += dx;
        y += dy;
    }
    return false;
}

// Determine if the enemy is in checkmate, stalemate, or in check.
int ChessGame::determineMate() {
    // Determine which player is the enemy, i.e who the current player tries
    // to put in checkmate.
    Player enemy = static_cast<Player>(1 - playerTurn());
    // Find the position of the enemy king.
    Position enemyKingPos = findKingPosition(enemy);
    // Get the enemy king.
    Piece* enemyKing = getPiece(enemyKingPos);

    // Determine if the enemy king is in check.
    std::vector<Piece*> killers = isUnderAttack(enemyKingPos, enemy, true);
    if (killers.size() > 0) {
        m_checkStatus.at(enemy) = true;
    }

    // Determine if the enemy king cannot move to the surrounding spaces.
    // If not, i.e spreadCheck returns true, then we do not continue determining
    // checkmate nor stalemate.
    // If spreadCheck returns true, then the king has places to go.
    // Otherwise, this is a flag for either Checkmate or Stalemate.
    if (spreadCheck(enemyKingPos, enemyKing)) {
        // Regular check.
        if (m_checkStatus.at(enemy)) return MOVE_CHECK;
        // The king is not in check nor any other special condition.
        else return SUCCESS;
    } else {
        if (killers.size() > 1) {
            // Multiple check, automatic checkmate.
            // (This is because unless the king is next to a checking piece,
            // you can't address both checks at once. We already made sure that
            // the king is not next to a checking piece.)
            return MOVE_CHECKMATE;
        } else if (killers.size() == 1) {
            // Only 1 piece is checking the enemy king.
            Piece* killer = killers.at(0);
            // See if the killing piece can itself be killed or blocked.
            if (canBlockCheck(getPosition(killer), enemyKingPos, playerTurn())) {
                // If any enemy pieces can block the killer, it is not Checkmate.
                return MOVE_CHECK;
            }
            // Otherwise, it is.
            return MOVE_CHECKMATE;
        } else {   // Not in check, so determine Stalemate
            return determineStalemate(enemy);
        }
    }
}

// Determine if a player is now in stalemate.
int ChessGame::determineStalemate(Player player) {
    // If any of the enemy pieces can make a move at all, it is not a Stalemate.
    for (Piece* piece : m_pieces) {
        if (((piece != nullptr) && (piece->owner() == player))) {
            if (spreadCheck(getPosition(piece), piece)) {
                // We have found a piece that has valid moves.
                return SUCCESS;
            }
        }
    }
    // All pieces of the player do not have valid moves.
    return MOVE_STALEMATE;
}

// Setup the chess board with its initial pieces
void ChessGame::setupBoard() {
    std::vector<int> pieces {
        ROOK_ENUM, KNIGHT_ENUM, BISHOP_ENUM, QUEEN_ENUM,
        KING_ENUM, BISHOP_ENUM, KNIGHT_ENUM, ROOK_ENUM
    };
    for (size_t i = 0; i < pieces.size(); ++i) {
        initPiece(PAWN_ENUM, WHITE, Position(i, 1));
        initPiece(pieces[i], WHITE, Position(i, 0));
        initPiece(pieces[i], BLACK, Position(i, 7));
        initPiece(PAWN_ENUM, BLACK, Position(i, 6));
    }
    // Start each player's check status as false
    m_checkStatus.at(WHITE) = false;
    m_checkStatus.at(BLACK) = false;
}

// Setup the chess board from a save file
void ChessGame::loadBoard(int turn, std::vector<std::tuple<int, int, int, int>>& gameData) {
    m_turn = turn;
    using GameDataIterator = std::vector<std::tuple<int, int, int, int>>::iterator;
    for (GameDataIterator it = gameData.begin(); it != gameData.end(); it++) {
        initPiece(std::get<3>(*it), static_cast<Player>(std::get<0>(*it)),
                  Position(std::get<1>(*it), std::get<2>(*it)));
    }
    // Check if either player is under check.
    for (int i = 0; i < 2; ++i) {
        Player player = static_cast<Player>(i);
        if (isUnderAttack(findKingPosition(player), player, true).size() > 0) {
            // If the player is under check.
            m_checkStatus.at(i) = true;
        } else {
            m_checkStatus.at(i) = false;
        }
    }
}
