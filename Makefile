CXX = g++
CXXFLAGS = -Wall -Wextra -pedantic -std=c++11 -g

# List of all (common) .o files (for executable dependencies)
# Board.o Chess.o FileIO.o Interface.o
# List of all .h files (for object file dependencies)
# Chess.h Interface.h FileIO.h Game.h Prompts.h Terminal.h

# Default target
chess: Main.o Board.o Chess.o FileIO.o Interface.o
	$(CXX) $(CXXFLAGS) -o Main Main.o Board.o Chess.o FileIO.o Interface.o

unittest: Unittest.o Board.o Chess.o FileIO.o Interface.o
	$(CXX) $(CXXFLAGS) -o Unittest Unittest.o Board.o Chess.o FileIO.o Interface.o

Main.o: Main.cpp Chess.h Interface.h FileIO.h Game.h Prompts.h Terminal.h
	$(CXX) $(CXXFLAGS) -c Main.cpp

Unittest.o: Unittest.cpp Chess.h Interface.h FileIO.h Game.h Prompts.h Terminal.h
	$(CXX) $(CXXFLAGS) -c Unittest.cpp

Board.o: Board.cpp Chess.h Interface.h FileIO.h Game.h Prompts.h Terminal.h
	$(CXX) $(CXXFLAGS) -c Board.cpp

Chess.o: Chess.cpp Chess.h Interface.h FileIO.h Game.h Prompts.h Terminal.h
	$(CXX) $(CXXFLAGS) -c Chess.cpp

FileIO.o: FileIO.cpp Chess.h Interface.h FileIO.h Game.h Prompts.h Terminal.h
	$(CXX) $(CXXFLAGS) -c FileIO.cpp

Interface.o: Interface.cpp Chess.h Interface.h FileIO.h Game.h Prompts.h Terminal.h
	$(CXX) $(CXXFLAGS) -c Interface.cpp

clean:
	rm *.o chess test
