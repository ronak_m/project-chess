#ifndef FILEIO_H
#define FILEIO_H

#include <exception>
#include <fstream>
#include <vector>

#include "Game.h"

class FileHandler {
public:
    typedef std::pair<int, std::vector<std::tuple<int, int, int, int>>> GameData;

    // Constructor to be used in-game.
    FileHandler(bool isRead);

    // This constructor is to be used for debugging only!
    FileHandler(std::string& fileName);

    // Deconstructor; it always closes an open file.
    ~FileHandler() {
        if (m_fs.is_open()) m_fs.close();
    }

    // Save file reading method.
    GameData readFile();

    // Save file write method.
    void writeFile(int width, int turn, std::vector<Piece*>& pieces);

private:
    // Filestream that FileHandler wraps.
    std::fstream m_fs;
};
#endif
