#include <iostream>
#include <string>

#include "Game.h"
#include "Chess.h"
#include "FileIO.h"
#include "Interface.h"
#include "Prompts.h"
#include "Terminal.h"

// Test methods from GameInterface
class InterfaceTest {
public:

    // Test the class GameInterface in Interface.h.
    void testGameInterface() {
        // Test getStartMenuInput in GameInterface
        std::cout << "Testing getStartMenuInput" << std::endl;
        int menuInput = GameInterface::getStartMenuInput();
        // The input is only valid if it's 1 or 2.
        if (menuInput == 1 || menuInput == 2) {
            std::cout << "Test passed!" << std::endl;
        } else {
            std::cout << "Test failed!" << std::endl;
        }

        // Test ioInput in GameInterface
        std::cout << "Testing ioInput" << std::endl;
        std::fstream fs1 = GameInterface::ioInput(true);
        if (fs1.is_open()) fs1.close();
        std::fstream fs2 = GameInterface::ioInput(false);
        if (fs2.is_open()) fs2.close();

        // Test getIngameInput in GameInterface
        std::cout << "Testing getIngameInput in GameInterface" << std::endl;
        std::string input = GameInterface::getIngameInput(WHITE, 1);
        std::cout << "Test reached end!" << std::endl;
    }
};

// Test validMove methods.
class ValiditiyTest {
public:
    // Test possible movements of the Pawn piece
    void testPawnValidity() {
        // Load and set up the chessboard.
        ChessGame chess;
        std::string str("Unittest_Files/TestPawn.txt");
        FileHandler fh(str);
        FileHandler::GameData gd = fh.readFile();
        chess.loadBoard(gd.first, gd.second);
        std::cout << "Testing Pawn." << std::endl;

        // Display the chessboard.
        BoardDisplay bd(chess);
        bd.toggleDisplay();
        bd.displayBoard();

        /* This test involves 6 Pawns:
         * 1. A White Pawn on b2 (1, 1), with nothing to capture.
         * 2. A White Pawn on e3 (4, 2), with two Black pawns to capture.
         * 3. A White Pawn on h2 (7, 1), with two White pawns in front.
         * 3. A Black Pawn on b7 (1, 6), with nothing to capture
         * 4. A Black Pawn on e6 (4, 5), with two White pawns to capture.
         * 6. A Black Pawn on h7 (7, 6), with one Black pawn directly in front.
         */

        // Pawn 1
        Position start1(1, 1);
        Piece* pawn1 = chess.getPiece(start1);
        std::vector<Position> endPos1;
        // Add various end positions, both valid and invalid.
        endPos1.push_back(Position(1, 0)); // Move directly backwards. (ILLEGAL)
        endPos1.push_back(Position(0, 0)); // Move backwards and to the left. (ILLEGAL)
        endPos1.push_back(Position(2, 0)); // Move backwards and to the right. (ILLEGAL)
        endPos1.push_back(Position(0, 1)); // Move to the left. (ILLEGAL)
        endPos1.push_back(Position(2, 1)); // Move to the right. (ILLEGAL)
        endPos1.push_back(Position(0, 2)); // Move forwards and to the left. (ILLEGAL)
        endPos1.push_back(Position(3, 2)); // Move forwards and to the right. (ILLEGAL)
        endPos1.push_back(Position(1, 2)); // Move forwards by 1. (LEGAL)
        endPos1.push_back(Position(1, 3)); // Move forwards by 2. (LEGAL)
        // Do the tests
        for (auto it = endPos1.begin(); it != endPos1.end(); ++it) {
            std::cout << "Pawn 1, Test Move To (" << it->x << ", " << it->y << "): ";
            int gameStatus = pawn1->validMove(start1, *it, chess);
            if (gameStatus == SUCCESS) std::cout << "Regular move success.\n";
            else GameInterface::displayMoveMsg(gameStatus, WHITE);
        }

        // Pawn 2
        Position start2(4, 2);
        Piece* pawn2 = chess.getPiece(start2);
        std::vector<Position> endPos2;
        // Add various end positions, both valid and invalid.
        endPos2.push_back(Position(3, 3)); // Move forwards and to the left. (LEGAL)
        endPos2.push_back(Position(5, 3)); // Move forwards and to the right. (LEGAL)
        endPos2.push_back(Position(4, 3)); // Move forwards by one. (LEGAL)
        endPos2.push_back(Position(4, 4)); // Move forwards by two. (ILLEGAL)
        // Do the tests
        for (auto it = endPos2.begin(); it != endPos2.end(); ++it) {
            std::cout << "Pawn 2, Test Move To (" << it->x << ", " << it->y << "): ";
            int gameStatus = pawn2->validMove(start2, *it, chess);
            if (gameStatus == SUCCESS) std::cout << "Regular move success.\n";
            else GameInterface::displayMoveMsg(gameStatus, WHITE);

        }

        // Pawn 3
        Position start3(7, 1);
        Piece* pawn3 = chess.getPiece(start3);
        std::vector<Position> endPos3;
        // Add various end positions, both valid and invalid.
        endPos3.push_back(Position(7, 2)); // Move forwards by 1 (BLOCKED)
        endPos3.push_back(Position(7, 3)); // Move forwards by 2 (BLOCKED)
        // Do the tests
        for (auto it = endPos3.begin(); it != endPos3.end(); ++it) {
            std::cout << "Pawn 3, Test Move To (" << it->x << ", " << it->y << "): ";
            int gameStatus = pawn3->validMove(start3, *it, chess);
            if (gameStatus == SUCCESS) std::cout << "Regular move success.\n";
            else GameInterface::displayMoveMsg(gameStatus, WHITE);
        }

        // Pawn 4
        Position start4(1, 6);
        Piece* pawn4 = chess.getPiece(start4);
        std::vector<Position> endPos4;
        // Add various end positions, both valid and invalid.
        endPos4.push_back(Position(1, 7)); // Move directly backwards. (ILLEGAL)
        endPos4.push_back(Position(0, 7)); // Move backwards and to the left. (ILLEGAL)
        endPos4.push_back(Position(2, 7)); // Move backwards and to the right. (ILLEGAL)
        endPos4.push_back(Position(0, 6)); // Move forwards and to the left. (ILLEGAL)
        endPos4.push_back(Position(3, 6)); // Move forwards and to the right. (ILLEGAL)
        endPos4.push_back(Position(1, 5)); // Move forwards by 1. (LEGAL)
        endPos4.push_back(Position(1, 4)); // Move forwards by 2. (LEGAL)
        // Do the tests
        for (auto it = endPos4.begin(); it != endPos4.end(); ++it) {
            std::cout << "Pawn 4, Test Move To (" << it->x << ", " << it->y << "): ";
            int gameStatus = pawn4->validMove(start4, *it, chess);
            if (gameStatus == SUCCESS) std::cout << "Regular move success.\n";
            else GameInterface::displayMoveMsg(gameStatus, BLACK);

        }

        // Pawn 5
        Position start5(4, 5);
        Piece* pawn5 = chess.getPiece(start5);
        std::vector<Position> endPos5;
        // Add various end positions, both valid and invalid.
        endPos5.push_back(Position(3, 4)); // Move forwards and to the left. (LEGAL)
        endPos5.push_back(Position(5, 4)); // Move forwards and to the right. (LEGAL)
        endPos5.push_back(Position(4, 4)); // Move forwards by one. (LEGAL)
        endPos5.push_back(Position(4, 3)); // Move forwards by two. (ILLEGAL)
        // Do the tests
        for (auto it = endPos5.begin(); it != endPos5.end(); ++it) {
            std::cout << "Pawn 5, Test Move To (" << it->x << ", " << it->y << "): ";
            int gameStatus = pawn5->validMove(start5, *it, chess);
            if (gameStatus == SUCCESS) std::cout << "Regular move success.\n";
            else GameInterface::displayMoveMsg(gameStatus, BLACK);
        }

        // Pawn 6
        Position start6(7, 6);
        Piece* pawn6 = chess.getPiece(start6);
        std::vector<Position> endPos6;
        // Add various end positions, both valid and invalid.
        endPos6.push_back(Position(7, 5)); // Move forwards by 1 (BLOCKED)
        endPos6.push_back(Position(7, 4)); // Move forwards by 2 (BLOCKED)
        // Do the tests
        for (auto it = endPos6.begin(); it != endPos6.end(); ++it) {
            std::cout << "Pawn 6, Test Move To (" << it->x << ", " << it->y << "): ";
            int gameStatus = pawn6->validMove(start6, *it, chess);
            if (gameStatus == SUCCESS) std::cout << "Regular move success.\n";
            else GameInterface::displayMoveMsg(gameStatus, BLACK);

        }
        // Display final message.
        std::cout << "Pawn tests over!\n";
    }

    // Test the valid moves of the knight.
    void testKnightValidity() {
        // Load and set up the chessboard.
        ChessGame chess;
        std::string str("Unittest_Files/TestKnight.txt");
        FileHandler fh(str);
        FileHandler::GameData gd = fh.readFile();
        chess.loadBoard(gd.first, gd.second);
        std::cout << "Testing Knight." << std::endl;

        // Display the chessboard.
        BoardDisplay bd(chess);
        bd.toggleDisplay();
        bd.displayBoard();

        /* This test involves two Knights, both undergoing the same tests.
         * Knight 1 is located at c3 (2, 2) and is blocked by nothing.
         * Knight 2 is located at 6f (5, 5) and has most of its valid end positions blocked.
         */

        // Knight 1
        Position start1(2, 2);
        Piece* knight1 = chess.getPiece(start1);
        std::vector<Position> endPos1;
        // Create the end positions as a 5x5 square centered on the knight.
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                endPos1.push_back(Position(i, j));
            }
        }
        // Test all of the end positions.
        for (auto it = endPos1.begin(); it != endPos1.end(); ++it) {
            std::cout << "Knight 1, Test Move To (" << it->x << ", " << it->y << "): ";
            int gameStatus = knight1->validMove(start1, *it, chess);
            if (gameStatus == SUCCESS) std::cout << "Regular move success.\n";
            else GameInterface::displayMoveMsg(gameStatus, WHITE);
        }

        // Knight 2
        Position start2(5, 5);
        Piece* knight2 = chess.getPiece(start2);
        std::vector<Position> endPos2;
        // Create the end positions as a 5x5 square centered on the knight.
        for (int i = 3; i < 8; ++i) {
            for (int j = 3; j < 8; ++j) {
                endPos2.push_back(Position(i, j));
            }
        }
        // Test all of the end positions.
        for (auto it = endPos2.begin(); it != endPos2.end(); ++it) {
            std::cout << "Knight 2, Test Move To (" << it->x << ", " << it->y << "): ";
            int gameStatus = knight2->validMove(start2, *it, chess);
            if (gameStatus == SUCCESS) std::cout << "Regular move success.\n";
            else GameInterface::displayMoveMsg(gameStatus, WHITE);
        }
    }

    // Test the valid moves of the Bishop.
    void testBishopValidity() {
        // Load and set up the chessboard.
        ChessGame chess;
        std::string str("Unittest_Files/TestBishop.txt");
        FileHandler fh(str);
        FileHandler::GameData gd = fh.readFile();
        chess.loadBoard(gd.first, gd.second);
        std::cout << "Testing Bishop." << std::endl;

        // Display the chessboard.
        BoardDisplay bd(chess);
        bd.toggleDisplay();
        bd.displayBoard();

        /* This test will involve testing a 5x5 square centered on the bishop.
         * The bishop is located at d4 (3, 3).
         * A friendly piece will be located at e5 (4, 4).
         * An enemy piece will be located at e3 (4, 2).
         */

        Position start(3, 3);
        Piece* bishop = chess.getPiece(start);
        std::vector<Position> endPos;
        // Create the end positions as a 5x5 square centered on the bishop.
        for (int i = 1; i < 6; ++i) {
            for (int j = 1; j < 6; ++j) {
                endPos.push_back(Position(i, j));
            }
        }
        // Test all of the end positions.
        for (auto it = endPos.begin(); it != endPos.end(); ++it) {
            std::cout << "Bishop, Test Move To (" << it->x << ", " << it->y << "): ";
            int gameStatus = bishop->validMove(start, *it, chess);
            if (gameStatus == SUCCESS) std::cout << "Regular move success.\n";
            else GameInterface::displayMoveMsg(gameStatus, WHITE);
        }
    }

    // Test the valid moves of the Rook.
    void testRookValidity() {
        // Load and set up the chessboard.
        ChessGame chess;
        std::string str("Unittest_Files/TestRook.txt");
        FileHandler fh(str);
        FileHandler::GameData gd = fh.readFile();
        chess.loadBoard(gd.first, gd.second);
        std::cout << "Testing Rook." << std::endl;

        // Display the chessboard.
        BoardDisplay bd(chess);
        bd.toggleDisplay();
        bd.displayBoard();

        /* This test will involve testing a 5x5 square centered on the rook.
         * The rook is located at d4 (3, 3).
         * A friendly piece will be located at d5 (3, 4).
         * An enemy piece will be located at d3 (3, 2).
         */

        Position start(3, 3);
        Piece* rook = chess.getPiece(start);
        std::vector<Position> endPos;
        // Create the end positions as a 5x5 square centered on the bishop.
        for (int i = 1; i < 6; ++i) {
            for (int j = 1; j < 6; ++j) {
                endPos.push_back(Position(i, j));
            }
        }
        // Test all of the end positions.
        for (auto it = endPos.begin(); it != endPos.end(); ++it) {
            std::cout << "Rook, Test Move To (" << it->x << ", " << it->y << "): ";
            int gameStatus = rook->validMove(start, *it, chess);
            if (gameStatus == SUCCESS) std::cout << "Regular move success.\n";
            else GameInterface::displayMoveMsg(gameStatus, WHITE);
        }
    }

    // Test the valid moves of the Queen.
    void testQueenValidity() {
        // Load and set up the chessboard.
        ChessGame chess;
        std::string str("Unittest_Files/TestQueen.txt");
        FileHandler fh(str);
        FileHandler::GameData gd = fh.readFile();
        chess.loadBoard(gd.first, gd.second);
        std::cout << "Testing Queen." << std::endl;

        // Display the chessboard.
        BoardDisplay bd(chess);
        bd.toggleDisplay();
        bd.displayBoard();

        /* This test will involve testing a 5x5 square centered on the queen.
         * The queen is located at d4 (3, 3).
         * Friendly pieces will be located at d5 (3, 4) and e5 (4, 4).
         * Enemy pieces will be located at d3 (3, 2) and e3 (4, 2).
         */

        Position start(3, 3);
        Piece* queen = chess.getPiece(start);
        std::vector<Position> endPos;
        // Create the end positions as a 5x5 square centered on the bishop.
        for (int i = 1; i < 6; ++i) {
            for (int j = 1; j < 6; ++j) {
                endPos.push_back(Position(i, j));
            }
        }

        // Test all of the end positions.
        for (auto it = endPos.begin(); it != endPos.end(); ++it) {
            std::cout << "Queen, Test Move To (" << it->x << ", " << it->y << "): ";
            int gameStatus = queen->validMove(start, *it, chess);
            if (gameStatus == SUCCESS) std::cout << "Regular move success.\n";
            else GameInterface::displayMoveMsg(gameStatus, WHITE);
        }
    }

    // Test the valid moves of the King.
    void testKingValidity() {
        // Load and set up the chessboard.
        ChessGame chess;
        std::string str("Unittest_Files/TestKing.txt");
        FileHandler fh(str);
        FileHandler::GameData gd = fh.readFile();
        chess.loadBoard(gd.first, gd.second);
        std::cout << "Testing King." << std::endl;

        // Display the chessboard.
        BoardDisplay bd(chess);
        bd.toggleDisplay();
        bd.displayBoard();

        /* This test will involve testing a 5x5 square centered on the king.
         * The king is located at d4 (3, 3).
         * Friendly pieces will be located at d5 (3, 4) and e5 (4, 4).
         * Enemy pieces will be located at d3 (3, 2) and e3 (4, 2).
         */

        Position start(3, 3);
        Piece* king = chess.getPiece(start);
        std::vector<Position> endPos;
        // Create the end positions as a 5x5 square centered on the bishop.
        for (int i = 1; i < 6; ++i) {
            for (int j = 1; j < 6; ++j) {
                endPos.push_back(Position(i, j));
            }
        }

        // Test all of the end positions.
        for (auto it = endPos.begin(); it != endPos.end(); ++it) {
            std::cout << "King, Test Move To (" << it->x << ", " << it->y << "): ";
            int gameStatus = king->validMove(start, *it, chess);
            if (gameStatus == SUCCESS) std::cout << "Regular move success.\n";
            else GameInterface::displayMoveMsg(gameStatus, WHITE);
        }
    }
};

// Test special gamerules like castling and checkmate.
class GameruleTest {
public:

    // Test the validity of the castling functionality of the King.
    void testCastleValidity() {
        // Load and set up the chessboard.
        ChessGame chess;
        std::string str("Unittest_Files/TestCastle.txt");
        FileHandler fh(str);
        FileHandler::GameData gd = fh.readFile();
        chess.loadBoard(gd.first, gd.second);
        std::cout << "Testing Castle Validity." << std::endl;

        // Display the chessboard.
        BoardDisplay bd(chess);
        bd.toggleDisplay();
        bd.displayBoard();

        /* This test will involve 3 kings.
         * King 1 is located at e1 (4, 0) and is white.
         * A Rook is placed at h1 (7, 0) to permit kingside castling, but not at a1 (0, 0)
         * King 2 is located at e8 (4, 7) and is black.
         * Rooks will be placed at both a8 (0, 7) and h8 (7, 7)
         * But a piece will be placed on f8 (5, 7), as to only permit queenside castles.
         * King 3 will be located at d3 (3, 2) and thus cannot castle.
         */

        // Create King 1.
        Position start1(4, 0);
        Piece* king1 = chess.getPiece(start1);

        // Attempt castling queenside.
        std::cout << "Testing castling white king queenside: ";
        int castleAttempt1 = king1->validMove(start1, Position(2, 0), chess);
        if (castleAttempt1 == MOVE_CASTLE) {
            std::cout << "Successfully can castle.\n";
        } else GameInterface::displayMoveMsg(castleAttempt1, WHITE);

        // Attempt castling kingside.
        std::cout << "Testing castling white king kingside: ";
        int castleAttempt2 = king1->validMove(start1, Position(6, 0), chess);
        if (castleAttempt2 == MOVE_CASTLE) {
            std::cout << "Successfully can castle.\n";
        } else GameInterface::displayMoveMsg(castleAttempt2, WHITE);

        // Move king normally.
        std::cout << "Testing moving white king one square forwards: ";
        int castleAttempt3 = king1->validMove(start1, Position(4, 1), chess);
        if (castleAttempt3 == SUCCESS) {
            std::cout << "Successfully checked non-castle move.\n";
        } else GameInterface::displayMoveMsg(castleAttempt3, WHITE);

        // Create King 2.
        Position start2(4, 7);
        Piece* king2 = chess.getPiece(start2);

        // Attempt castling queenside.
        std::cout << "Testing castling black king queenside: ";
        int castleAttempt4 = king2->validMove(start2, Position(2, 7), chess);
        if (castleAttempt4 == MOVE_CASTLE) {
            std::cout << "Successfully can castle.\n";
        } else GameInterface::displayMoveMsg(castleAttempt4, WHITE);

        // Attempt castling kingside.
        std::cout << "Testing castling black king kingside: ";
        int castleAttempt5 = king2->validMove(start2, Position(6, 7), chess);
        if (castleAttempt5 == MOVE_CASTLE) {
            std::cout << "Successfully can castle.\n";
        } else GameInterface::displayMoveMsg(castleAttempt5, WHITE);

    }

    // Test the spreadCheck method on various pieces.
    void testSpreadCheck() {
        // Load and set up the chessboard.
        ChessGame chess;
        std::string str("Unittest_Files/TestSpread.txt");
        FileHandler fh(str);
        FileHandler::GameData gd = fh.readFile();
        chess.loadBoard(gd.first, gd.second);
        std::cout << "Testing Spread Check." << std::endl;

        // Display the chessboard.
        BoardDisplay bd(chess);
        bd.toggleDisplay();
        bd.displayBoard();

        /* We test spreadCheck on a rook, bishop, king, and queen.
         * The Rook is located at b2 (1, 1).
         * The Bishop is located at g2 (6, 1).
         * The King is located at b7 (1, 6).
         * The Queen is located at g7 (6, 6).
         * We surround the pieces completely with Knights to always return false.
         */

        // Create the approrpiate pieces.
        Position rookStart(1, 1);
        Piece* rook = chess.getPiece(rookStart);

        Position bishopStart(6, 1);
        Piece* bishop = chess.getPiece(bishopStart);

        Position kingStart(1, 6);
        Piece* king = chess.getPiece(kingStart);

        Position queenStart(6, 6);
        Piece* queen = chess.getPiece(queenStart);

        // Test spreadCheck with everything false (i.e. 0).
        std::cout << "Rook Result 1: " << chess.spreadCheck(rookStart, rook) << "\n";
        std::cout << "Bishop Result 1: " << chess.spreadCheck(bishopStart, bishop) << "\n";
        std::cout << "King Result 1: " << chess.spreadCheck(kingStart, king) << "\n";
        std::cout << "Queen Result 1: " << chess.spreadCheck(queenStart, queen) << "\n";
    }

    // Test check both by testing isUnderAttack itself and by testing determineMate.
    void testCheck() {
        // Load and set up the chessboard.
        ChessGame chess;
        std::string str("Unittest_Files/TestAttack.txt");
        FileHandler fh(str);
        FileHandler::GameData gd = fh.readFile();
        chess.loadBoard(gd.first, gd.second);
        std::cout << "Testing Check." << std::endl;

        // Display the chessboard.
        BoardDisplay bd(chess);
        bd.toggleDisplay();
        bd.displayBoard();

        /* There are two killer pieces and one victim piece.
         * The White Bishop at h4 (7, 3) is a killer piece.
         * The White Rook at d4 (3, 3) is a killer piece.
         * The Black King at d8 (3, 7) is the victim piece.
         * We also attempt to attack e1 (4, 0), which is empty.
         */

        // Test these particular positions if they're under attack by white.
        // The test should print the number of pieces attacking the position.
        std::cout << "Attacking h4: " << chess.isUnderAttack(Position(3, 7), BLACK, true).size()
                  << "\n";
        std::cout << "Attacking e1: " << chess.isUnderAttack(Position(4, 0), BLACK, true).size()
                  << "\n";
        std::cout << "Attacking b2: " << chess.isUnderAttack(Position(1, 1), BLACK, true).size()
                  << "\n";

        // Now test that White is checking Black's king.
        // Note that the turn is set to 1 in this scenario, i.e. at White's turn.
        int checkStatus = chess.determineMate();
        GameInterface::displayMoveMsg(checkStatus, WHITE);
    }

    // Test whether the king is in checkmate.
    void testCheckMate() {
        // In this test we will test multiple instances of ChessGame.
        std::cout << "Testing checkmate." << std::endl;

        int numChessGames = 9;
        // We set up the different ChessGames.
        std::vector<ChessGame*> chessGames;
        for (int i = 0; i < numChessGames; ++i) {
            std::string str("Unittest_Files/Checkmate_Files/TestCheckmate" + std::to_string(i) + ".txt");
            FileHandler fh(str);
            FileHandler::GameData gd = fh.readFile();
            ChessGame* chess = new ChessGame();
            chess->loadBoard(gd.first, gd.second);
            chessGames.push_back(chess);
        }

        // We test and display them.
        for (auto it = chessGames.begin(); it != chessGames.end(); ++it) {
            // Display the board.
            BoardDisplay bd(**it);
            bd.toggleDisplay();
            bd.displayBoard();

            // Determine that White puts Black in checkmate.
            int checkStatus = (*it)->determineMate();
            switch (checkStatus) {
            case MOVE_CHECKMATE:
                Prompts::checkMate(WHITE);
                break;
            case MOVE_STALEMATE:
                Prompts::staleMate();
                break;
            case MOVE_CHECK:
                Prompts::check(WHITE);
                break;
            case SUCCESS:
                std::cout << "Black king is not in danger." << std::endl;
                break;
            default:
                break;
            }
        }

        // Free allocated memory.
        for (size_t i = 0; i < chessGames.size(); ++i) {
            ChessGame* chess = chessGames.at(i);
            delete chess;
        }
    }
};

int main() {
    /*InterfaceTest it;
    it.testGameInterface();

    ValiditiyTest vt;
    vt.testPawnValidity();
    vt.testKnightValidity();
    vt.testBishopValidity();
    vt.testRookValidity();
    vt.testQueenValidity();
    vt.testKingValidity();
    */
    GameruleTest gt;
    // gt.testCastleValidity();
    //gt.testSpreadCheck();
    //gt.testCheck();
    gt.testCheckMate();

    return 0;
}
