#include "Chess.h"
#include "Interface.h"
#include "Prompts.h"
#include "Terminal.h"

// STANDARD INPUT METHODS

// Recieve the user input from the start menu
int GameInterface::getStartMenuInput() {
    // The menu input, which is either "1" or "2"
    std::string menuInput;

    do {
        // Display the start menu
        Prompts::menu();
        // Attempt to take in input until it is either 1 or 2
        std::getline(std::cin, menuInput);
    } while (menuInput != "1" && menuInput != "2");

    // Return the input in numerical format
    return menuInput.at(0) - '1' + 1;
}

// Recieve input for loading or opening a save file and
// create a filestream object based on said input.
std::fstream GameInterface::ioInput(bool isRead) {
    // Display the prompt depending on if we're loading or saving
    if (isRead) {
        Prompts::loadGame();
    } else {
        Prompts::saveGame();
    }

    // Create a input filestream object
    std::fstream fs;
    // Get the filename from the user input
    std::string fileName;
    std::getline(std::cin, fileName);

    // Open and return the filestream object
    if (isRead) {
        fs.open(fileName, std::fstream::in);
    } else {
        fs.open(fileName, std::fstream::out);
    }
    return fs;
}

// Get ingame input, i.e. the input the player inputs at each game turn.
std::string GameInterface::getIngameInput(Player pl, int turn) {
    // String that will store user input
    std::string lineStr;
    // Bool for if the input is valid
    bool isInputValid = false;

    do {
        // Display prompt.
        Prompts::playerPrompt(pl, turn);
        // Read user input (specifically the entire line).
        std::getline(std::cin, lineStr);
        // Convert string to lowercase char by char (so that input is
        // case-insensitive).
        std::string::iterator it;
        for (it = lineStr.begin(); it != lineStr.end(); ++it) {
            *it = std::tolower(*it);
        }
        isInputValid = validateIngameInput(lineStr);
        // Display parseError propmpt upon invalid input.
        if (!isInputValid) Prompts::parseError();
    } while (!isInputValid);

    // Return transformed user input
    return lineStr;
}

// Validate that ingame input is valid, i.e. is a coordinate in algebraic chess notation
// or a proper non-move command.
bool GameInterface::validateIngameInput(std::string& lineStr) {
    // Return true if the user input is a proper non-move command.
    if (lineStr == "q" || lineStr == "board" || lineStr == "save" || lineStr == "forfeit") {
        return true;
    }

    // All further operations operate under the assumption that the user is
    // trying to input coordinates.
    // First, check that the length is appropriate.
    if (lineStr.size() != 5) {
        return false;
    }

    // Check that the two coordinates are separated by a space.
    if (lineStr.at(2) != ' ') {
        return false;
    }

    // Check that the x coordinates are in letter formate and the y coordinates
    // are in number format.
    if (!isalpha(lineStr.at(0)) || !isalpha(lineStr.at(3))) return false;
    if (!isdigit(lineStr.at(1)) || !isdigit(lineStr.at(4))) return false;

    // If all tests pass, then we're good.
    return true;
}

// Parse starting and ending board positions based off of (valid) user input
std::pair<Position, Position> GameInterface::parsePosition (std::string& lineStr) {
    //The array of unsigned integers
    std::array<int, 4> intArray;

    // Convert the letters of the chess algebraic notation into x-coordinates
    std::array<char, 2> charArray;
    charArray.at(0) = lineStr.at(0);
    charArray.at(1) = lineStr.at(3);
    for (size_t i = 0; i < charArray.size(); ++i) {
        // Convert char, e.g. 'a' - 'a' = 0
        intArray.at(2 * i) = charArray.at(i) - 'a';
    }

    // Fill in the rest of the integer array by turning the numerical
    // inputs into integral values.
    intArray.at(1) = lineStr.at(1) - '1';
    intArray.at(3) = lineStr.at(4) - '1';

    // Create and return the position structs
    Position startPos((unsigned)intArray.at(0), (unsigned)intArray.at(1));
    Position endPos((unsigned)intArray.at(2), (unsigned)intArray.at(3));
    std::pair<Position, Position> posPair = std::make_pair(startPos, endPos);
    return posPair;
}

// STANDARD OUTPUT METHODS

// Display the status of the move just performed.
void GameInterface::displayMoveMsg(int moveStatus, int player) {
    switch (moveStatus) {
    //std::cout << "Move Status: " << moveStatus << std::endl;
    case MOVE_CHECK:
        Prompts::check(static_cast<Player>(player));
        break;
    case MOVE_CAPTURE:
        Prompts::capture(static_cast<Player>(player));
        break;
    case MOVE_ERROR_OUT_OF_BOUNDS:
        Prompts::outOfBounds();
        break;
    case MOVE_ERROR_NO_PIECE:
        Prompts::noPiece();
        break;
    case MOVE_ERROR_ILLEGAL:
        Prompts::illegalMove();
        break;
    case MOVE_ERROR_BLOCKED:
        Prompts::blocked();
        break;
    case MOVE_ERROR_MUST_HANDLE_CHECK:
        Prompts::mustHandleCheck();
        break;
    case MOVE_ERROR_CANT_EXPOSE_CHECK:
        Prompts::cantExposeCheck();
        break;
    case MOVE_ERROR_CANT_CASTLE:
        Prompts::cantCastle();
        break;
    default:
        break;
    }
}

// Display an endgame message depending on how the game ended.
void GameInterface::displayEndMsg(int gameStatus, int player, int turn) {
    if (gameStatus != GAME_QUIT) { // If the player simply quits, display nothing.
        if (gameStatus == MOVE_CHECKMATE) {
            // Display that the player won.
            Prompts::checkMate(static_cast<Player>(player));
            Prompts::win(static_cast<Player>(player), turn);
        } else if (gameStatus == MOVE_STALEMATE) {
            Prompts::staleMate();
        } else {
            // Display that the player's opponent won.
            Prompts::win(static_cast<Player>(1 - player), turn);
        }
        Prompts::gameOver();
    }
}

// BOARD_DIPSLAY METHODS

// Construct the BoardDisplay with properties of the chessboard.
BoardDisplay::BoardDisplay(Board& board) {
    // Dimensions in terms of the number of "pixels," i.e. one char on the terminal,
    // based on the height and width of the board, which in Board are in terms of
    // squares. Note that 1 square = 2 pixels tall and 4 pixels wide.
    m_dispWidth = (board.width() + 2) * 4;
    m_dispHeight = (board.height() + 2) * 2;
    // Reference to the current board object.
    m_boardCopy = &board;
    // Initialize the seed to the current UNIX time.
    m_seed = std::time(nullptr);
}

// Display the board.
void BoardDisplay::displayBoard() {

    // If we do not want the bourd to be displayed, the method ends here.
    if (!m_isDisplayed) return;


    // Pixel darkness; alternating this will change the background of each "pixel"
    // and will create the checkerboard pattern.
    bool isDark = false; // We will be switching to false immediately.

    for (int i = 0; i < m_dispHeight; ++i) {
        // If we're on a new row, change pixel darkness.
        if (!(i % 2)) isDark = !isDark;
        for (int j = 0; j < m_dispWidth; ++j) {
            // If we reached a new square, change pixel darkness.
            if (!(j % 4)) {
                colorBackground(isDark, i, j);
            }
            // Depending on the row we are on, display the current "pixel" differntly.
            if (i == 0 || i == (m_dispHeight - 1)) {
                // For the very first and last rows, display the random pattern.
                drawRandomPattern();
            } else if (i == 1 || i == (m_dispHeight - 2)) {
                // For the second and penultimate rows, display the x-coordinates.
                displayCoordinates(j);
            } else {
                // Display a row of the chessboard proper.
                displayRegRow(i, j);
            }
        }
        // Move on to the next row.
        std::cout << std::endl;
    }
    // Reset the terminal's colors.
    Terminal::set_default();
}

// Draw a regular row, i.e. a row that includes the chessboard proper.
// What exactly is draw depends on the x-coordinate.
void BoardDisplay::displayRegRow (int i, int j) {
    // If we're in the first or last two columns.
    if (j < 2 || j > (m_dispWidth - 3)) {
        drawRandomPattern();
    } else if (j == 3 || j == (m_dispWidth - 4)) {
        // Draw the y-coordinate based on row i.
        drawYCoord(i);
    } else if (j == 2 || j == (m_dispWidth - 3)) {
        // If we're on the third or third-to-last column.
        std::cout << ' ';
    } else { // Display part of the chessboard proper.

        // Get the piece at the current position/square.
        Piece* p = m_boardCopy->getPiece(Position(j / 4 - 1, (m_boardCopy->width() - 1) - (i / 2 - 1)));

        // If there's a piece on that row, display it.
        if (p != nullptr) {
            // Display a piece symbol in the center four pixels of the 16x16 square.
            if (j % 4 == 1 || j % 4 == 2) {
                std::string unicodeRep = p->getUnicodeRep();
                // Display the white player's pieces as yellow and the black player's
                // as blue. Actual white and black pieces are too hard to see.
                if (p->owner() == WHITE) {
                    Terminal::colorFg(false, Terminal::Color::YELLOW);
                } else {
                    Terminal::colorFg(false, Terminal::Color::BLUE);
                }
                // Display the piece symbol.
                std::cout << unicodeRep;
            } else { // If a piece is on the square but we're on the outside 12 pixels.
                std::cout << ' ';
            }
        } else { // If there's no piece on the square.
            std::cout << ' ';
        }
    }
}

// Draw the x-coordinates. This is to help the players input the
// correct coordinates.
void BoardDisplay::displayCoordinates(int j) {
    // If we're in the correct columns
    if (j > 3 && j < 36) {
        // Display the current coordinates in the middle two pixels of the column.
        if ((j % 4 == 1) || (j % 4 == 2)) {
            drawXCoord(j);
        } else {
            std::cout << ' ';
        }
    } else if (j < 2 || j > (m_dispWidth - 3)) { // For the first and last two columns
        drawRandomPattern();
    } else {
        std::cout << ' ';
    }
}

// Color the background of the pixel based on if it's light or dark, and whether if
// it is part of an edge or not.
// Note: We need to pass isDark by reference, in order to change the original value.
void BoardDisplay::colorBackground(bool& isDark, int i, int j) {
    // Flip the brightness status.
    isDark = !isDark;
    // Determine if the current pixel is on a border.
    bool isEdge = determineEdge(i, j);
    // The color we will change the background to.
    Terminal::Color bgColor;
    // If we're on an edge, our choices are red and green.
    // Otherwise, they're (obviously) black or white.
    if (isEdge) {
        if (isDark) {
            bgColor = Terminal::Color::BLUE;
        } else {
            bgColor = Terminal::Color::YELLOW;
        }
    } else {
        if (isDark) {
            bgColor = Terminal::Color::BLACK;
        } else {
            bgColor = Terminal::Color::WHITE;
        }
    }
    // Change the background color.
    Terminal::colorBg(bgColor);
}

// Determine if the current "pixel" lies in the edge of the board.
bool BoardDisplay::determineEdge(int i, int j) {
    // The vertical edges include the first and last four pixel columns.
    // The horizontal edges include the first and last two pixl rows.
    if (i < 2 || i > (m_dispHeight - 3)) { // If we're on a horizontal edge.
        return true;
    } else if (j < 4 || j > (m_dispWidth - 5)) { // If we're on a vertical edge.
        return true;
    } else { // If we're not on an edge at all.
        return false;
    }
}

// Draw an instance of an alphanumeric x-coordinate.
void BoardDisplay::drawXCoord(int x) {
    // Draw the text in black.
    Terminal::colorFg(false, Terminal::Color::BLACK);
    // Determine the approrpiate coordinate based on the x coordinate.
    // Note that for the central chessboard (not the border), the coordinates
    // range from 1 to 8 inclusive.
    // E.g. for first column, (1/4)-1 + 'A' = 0 + 'A'
    char coord = x / 4 - 1 + 'A';
    std::cout << coord;
}

// Draw an instance of a numerical y-coordinate.
void BoardDisplay::drawYCoord(int y) {
    // Draw the text in black.
    Terminal::colorFg(false, Terminal::Color::BLACK);
    // Determine the approrpiate coordinate based on the y coordinate.
    // Note that for the central chessboard (not the border), the coordinates
    // range from 1 to 8 inclusive.
    char coord = ((m_boardCopy->height() - 1) - (y / 2 - 1)) + '1';
    std::cout << coord;
}

// Return a random pattern (from the "Box Drawings" Unicode block)
// for the "pixel" given the random seed m_seed.
void BoardDisplay::drawRandomPattern() {
    // Draw the pattern in white.
    Terminal::colorFg(false, Terminal::Color::WHITE);
    // Set the random number generator with our seed.
    std::srand(m_seed);
    // Get a random number between 0 and 10 inclusive.
    int randNum = std::rand() % 11;
    // Based on the random number, return a random pattern.
    switch (randNum) {
    case 0:
        std::cout << "\u2550";
        break;
    case 1:
        std::cout << "\u2551";
        break;
    case 2:
        std::cout << "\u2554";
        break;
    case 3:
        std::cout << "\u2557";
        break;
    case 4:
        std::cout << "\u255a";
        break;
    case 5:
        std::cout << "\u255d";
        break;
    case 6:
        std::cout << "\u2560";
        break;
    case 7:
        std::cout << "\u2563";
        break;
    case 8:
        std::cout << "\u2566";
        break;
    case 9:
        std::cout << "\u2569";
        break;
    default: // Case 10
        std::cout << "\u256c";
        break;
    }
    // Change the seed by incrementing it by one, for next time.
    ++m_seed;
}

// Toggle method. Switches display on/off.
bool BoardDisplay::toggleDisplay() {
    m_isDisplayed = !m_isDisplayed;
    return m_isDisplayed;
}
