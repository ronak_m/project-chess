#ifndef CHESS_H
#define CHESS_H

#include "Game.h"
#include "Interface.h"

// Game status codes
// -----------------
// These enumerations are optional. You can choose to use them,
// or you can decide they're not needed. They *could* be used, for
// example, to return values from validMove() and makeMove(), and
// any other methods you want. But so long as you follow the conventions
// of those methods (>0 is success, <0 is failure), you're free to
// do things your own way.
enum status {
    LOAD_FAILURE = -10,
    SAVE_FAILURE,
    PARSE_ERROR,
    MOVE_ERROR_OUT_OF_BOUNDS,
    MOVE_ERROR_NO_PIECE,
    MOVE_ERROR_BLOCKED,
    MOVE_ERROR_CANT_CASTLE,
    MOVE_ERROR_MUST_HANDLE_CHECK,
    MOVE_ERROR_CANT_EXPOSE_CHECK,
    MOVE_ERROR_ILLEGAL,
    // Do not put anything equal 0. This is a neutral state.
    SUCCESS = 1,
    MOVE_CASTLE,
    MOVE_CHECK,
    MOVE_CAPTURE,
    MOVE_CHECKMATE,
    MOVE_STALEMATE,
    GAME_QUIT,
    GAME_FORFEIT,
};

// Possible pieces
enum PieceEnum {
    PAWN_ENUM = 0,
    ROOK_ENUM,
    KNIGHT_ENUM,
    BISHOP_ENUM,
    QUEEN_ENUM,
    KING_ENUM
};

class Pawn : public Piece {
protected:
    friend PieceFactory<Pawn>;
    Pawn(Player owner, int id) : Piece(owner, id) {}
public:
    // This method will have piece-specific logic for checking valid moves
    // It may also call the generic Piece::validMove for common logic
    int validMove(Position start, Position end, const Board& board) const override;

    // Return the Unicode representation for board display
    std::string getUnicodeRep() const override {
        return "\u265f";
    }
};

class Rook : public Piece {
protected:
    friend PieceFactory<Rook>;
    Rook(Player owner, int id) : Piece(owner, id) {}
public:
    // This method will have piece-specific logic for checking valid moves
    // It may also call the generic Piece::validMove for common logic
    int validMove(Position start, Position end, const Board& board) const override;

    // Return the Unicode representation for board display
    virtual std::string getUnicodeRep() const override {
        return "\u265c";
    }
};

class Knight : public Piece {
protected:
    friend PieceFactory<Knight>;
    Knight(Player owner, int id) : Piece(owner, id) {}
public:
    // This method will have piece-specific logic for checking valid moves
    // It may also call the generic Piece::validMove for common logic
    int validMove(Position start, Position end, const Board& board) const override;

    // Return the Unicode representation for board display
    virtual std::string getUnicodeRep() const override {
        return "\u265e";
    }
};

class Bishop : public Piece {
protected:
    friend PieceFactory<Bishop>;
    Bishop(Player owner, int id) : Piece(owner, id) {}
public:
    // This method will have piece-specific logic for checking valid moves
    // It may also call the generic Piece::validMove for common logic
    int validMove(Position start, Position end, const Board& board) const override;

    // Return the Unicode representation for board display
    virtual std::string getUnicodeRep() const override {
        return "\u265d";
    }
};

class Queen : public Piece {
protected:
    friend PieceFactory<Queen>;
    Queen(Player owner, int id) : Piece(owner , id) {}
public:
    // This method will have piece-specific logic for checking valid moves
    // It may also call the generic Piece::validMove for common logic
    int validMove(Position start, Position end, const Board& board) const override;

    // Return the Unicode representation for board display
    virtual std::string getUnicodeRep() const override {
        return "\u265b";
    }
};

class King : public Piece {
protected:
    friend PieceFactory<King>;
    King(Player owner, int id) : Piece(owner, id) {}
public:
    // This method will have piece-specific logic for checking valid moves
    // It may also call the generic Piece::validMove for common logic
    int validMove(Position start, Position end, const Board& board) const override;

    // Method parallel to validMove that specifically checks if castling is
    // possible. Note that it does not take into account check.
    int validCastle(Position start, Position end, const Board& boad) const;

    // Return the Unicode representation for board display
    virtual std::string getUnicodeRep() const override {
        return "\u265a";
    }
};

class ChessGame : public Board {
public:
    ChessGame() : Board(8, 8) {
        // Add all factories needed to create Piece subclasses
        addFactory(new PieceFactory<Pawn>(PAWN_ENUM));
        addFactory(new PieceFactory<Rook>(ROOK_ENUM));
        addFactory(new PieceFactory<Knight>(KNIGHT_ENUM));
        addFactory(new PieceFactory<Bishop>(BISHOP_ENUM));
        addFactory(new PieceFactory<Queen>(QUEEN_ENUM));
        addFactory(new PieceFactory<King>(KING_ENUM));
    }

    // Setup the chess board with its initial pieces
    virtual void setupBoard();

    // Setup the chess board from a save file
    virtual void loadBoard(int turn, std::vector<std::tuple<int, int, int, int>>&);

    // Whether the chess game is over, getter and setter.
    virtual int gameOver() const override {
        return m_gameStatus;
    }

    // Set the game to be over.
    void endGame(status gameStatus) {
        m_gameStatus = gameStatus;
    }

    // Do a turn for the chess game, which can include performing a move, board
    // display, and saving.
    // The method returns and integer with the status
    // >= is SUCCESS, < 0 is failure.
    virtual void doTurn() override;

    // Perform a move from the start Position to the end Position
    // The method returns an integer with the status
    // >= 0 is SUCCESS, < 0 is failure
    virtual int makeMove(Position start, Position end) override;

    // Perform a castle.
    // >= 0 is SUCCESS, < 0 is failure.
    int makeCastle(Player myPlayer, Position start, Position end, Piece* king);

    // Promote the Pawn to a Queen if it reaches the last row of the chessboard.
    void promotePawn(Position pawnPos, Player player);

    // Determine if a move by the player will result in their king being exposed
    // to check.
    virtual int determineCheck(Player player, Position start, Position end, Piece* piece);

    // Determine if a particular piece is able to move at all.
    // This is used to determine if the King is in checkmate, or if the game
    // ends in stalemate.
    bool spreadCheck(Position start, Piece* piece);

    // Find the position that are king is on. This is used to determine if the
    // king is in check.
    Position findKingPosition(Player myPlayer) const;

    // Determine if the position we are analyzing can be captured, i.e. is "in check."
    // This is usually used to determine if the king is in check, but can also be
    // used to determine if surrounding positions are illegal for a piece to move to.
    // The countPawns bool is for if we want to count a pawn's capture move (e.g. when
    // determining check) or not.
    std::vector<Piece*> isUnderAttack(Position pos, Player myPlayer, bool countPawns);

    // Determine if a piece that checks the enemy King can be blocked by an enemy piece.
    bool canBlockCheck(Position killer, Position target, Player myPlayer);

    // Determine if we can put the enemy king in a checkmate, stalemate, or check
    // position.
    int determineMate();

    // Determine if a player is now in stalemate.
    int determineStalemate(Player player);

protected:
    // The check status of any of the players.
    // first entry <-> white; second entry <-> black.
    std::array<bool, 2> m_checkStatus;

    // Object to display the board when needed.
    BoardDisplay m_boardDisplay = BoardDisplay(*this);

    // Boolean to see if game is over.
    int m_gameStatus = 0;
};

#endif
