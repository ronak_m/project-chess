#include "FileIO.h"
#include "Interface.h"

// Regular constructor that is called in-game.
FileHandler::FileHandler(bool isRead) {
    // Get a file from user input.
    m_fs = GameInterface::ioInput(isRead);

    // If the file cannot be opened (e.g. is not found), throw an exception.
    if (!m_fs.is_open()) {
        throw std::runtime_error("Failed to open the file.");
    }
}

// Debug-only constructor.
FileHandler::FileHandler(std::string& fileName) : m_fs(fileName) {}

// Read from a savefile.
FileHandler::GameData FileHandler::readFile() {
    // Read the header of the save file. If it's not "chess", then the file
    // is invalid.
    std::string gameStr;
    m_fs >> gameStr;
    if (gameStr != "chess") throw std::invalid_argument("Failed to load the game.");

    // Read the string describing the last turn.
    std::string turnStr;
    m_fs >> turnStr;
    int turn = std::stoi(turnStr);

    // Read in the game data that describes the status of the pieces.
    // Integral data arranged as "owner, x pos, y pos, piece id".
    std::vector<std::tuple<int, int, int, int>> gameData;
    // Vector of strings read from game data.
    std::vector<std::string> gameDataStr;
    std::string currStr;
    // Read the game data in string format until the end of file.
    // Example set of strings: "0 a2 0\n".
    while (m_fs >> currStr) {
        gameDataStr.push_back(currStr);
    }

    // Iterate over each line (which each consist of three strings) to parse
    // them into integral data.
    using StrVectIterator = std::vector<std::string>::iterator;
    for (StrVectIterator it = gameDataStr.begin(); it != gameDataStr.end(); it += 3) {
        int ownerNum = std::stoi(*it);
        int xPos = (it + 1)->at(0) - 'a';
        int yPos = (it + 1)->at(1) - '1';
        int pieceId = std::stoi(*(it + 2));
        std::tuple<int, int, int, int> pieceData = std::make_tuple(ownerNum, xPos, yPos, pieceId);
        gameData.push_back(pieceData);
    }

    return std::make_pair(turn, gameData);
}

// Write to a savefile.
void FileHandler::writeFile(int width, int turn, std::vector<Piece*>& pieces) {
    // Write the header of the save file.
    m_fs << "chess" << std::endl; // The "chess" header.
    m_fs << turn << std::endl;    // The number of the last turn.

    // Write the piece gamedata into the save file.
    for (unsigned i = 0; i < pieces.size(); ++i) {
        Piece* p = pieces.at(i);
        if (p != nullptr) {
            // Get the internal piece data.
            int pieceId = p->id();
            int ownerNum = static_cast<int> (p->owner());
            // Get and convert the position of the piece.
            char xPosChar = i % width + 'a';
            char yPosChar = i / width + '1';
            m_fs << ownerNum << " " << xPosChar << yPosChar << " " << pieceId << std::endl;
        }
    }
}
