#ifndef INTERFACE_H
#define INTERFACE_H

#include <cctype>
#include <exception>
#include <iostream>
#include <locale>
#include <fstream>
#include <string>
#include <vector>

// Used to generate the board's random pattern.
#include <cstdlib>
#include <ctime>

#include "Game.h"
#include "Terminal.h"

// Utility class of static metods that focus on taking in
// and processing user input.
class GameInterface {
public:
    // Recieve the user input from the start menu
    static int getStartMenuInput();

    // Recieve input for loading or opening a save file and
    // create a filestream object based on said input.
    static std::fstream ioInput(bool isLoad);

    // Get ingame input, i.e. the input the player inputs at each game turn.
    static std::string getIngameInput(Player pl, int turn);

    // Parse starting and ending board positions based off of user input
    static std::pair<Position, Position> parsePosition (std::string& lineStr);

    // Display messages at the end of a move depending on move status.
    static void displayMoveMsg(int moveStatus, int player);

    // Display a message when the game ends or is exited.
    static void displayEndMsg(int gameStatus, int player, int turn);

private:
    // Validate ingame input
    static bool validateIngameInput(std::string& lineStr);
};

// Class that contains static methods to display the game board
class BoardDisplay {
public:
    BoardDisplay(Board& board);
    // Display the face at the top of the board depending on the game state
    void displayFace(int gameState);

    // Display the board proper with the current arrangement of Pieces
    void displayBoard();

    // Toggle the display variable.
    bool toggleDisplay();
private:
    // Member variables.

    // Height and width of the display in terms of "pixels"
    int m_dispHeight;
    int m_dispWidth;
    // The display's copy of the board (to get board info like piece position).
    Board* m_boardCopy;
    // The random seed used to generate the border pattern.
    int m_seed;
    // Whether the board is set to be displayed each turn or not.
    bool m_isDisplayed = false;

    // Display a row that includes the chessboard proper.
    void displayRegRow(int i, int j);

    // Display a row that displays the x-coordinates on the board.
    void displayCoordinates(int j);

    // Change the background on the current pixel.
    void colorBackground(bool& isDark, int i, int j);

    // Determine if the current pixel is part of a border.
    bool determineEdge(int i, int j);

    // Draw a pixel that displays an x-coordinate (from a-h).
    void drawXCoord(int j);

    // Draw a pixel that displays a y-coordinate (from 1-8).
    void drawYCoord(int i);

    // Draw a pixel that displays a randomly-selected Unicode pattern.
    void drawRandomPattern();
};

#endif
