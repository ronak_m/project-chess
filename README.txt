PROJECT CHESS README
Class: EN.600.120 Intermediate Programming
Date: May 5, 2017

TEAM:
Kelvin Qian - kqian2
Ronak Mehta - rmehta13

DESIGN:
Our design mostly preserves the original codebase while not only adding new classes, but also adding additional methods in the exiting classes required for a chess game. The program incorporates classes in the following ways:
 - Board: Implements functionality that's common to board games in this style (e.g. chess and checkers). 
 - ChessGame: Subclass of Board; implements functionality that is specific to the ruleset of chess.
 - Piece: Implements generic functionality for board-game pieces.
 - Piece classes: Chess-specific subclasses of Piece that have their own rulesets.
 - GameInterface: Takes in and parses input from the player, as well as handling most of the text-based output.
 - BoardDisplay: Contains board-related data and displays the board.
 - FileHandler: Wrapper of filestream object that can read and write save files.
 - Unittest classes: Contain methods that do unit tests on the game.

COMPLETNESS:
Our program implements complete functionality of a chess game, including castling, with the following exceptions:
 - The game does not implement functionalities not specified by the assignment, namely en passant, the 50-turn rule, and promoting pawns to non-queen pieces.
 - Saving does not save whether a piece has moved or not. This can result in exploits regarding castling by skilled players.
 - While we have extensively tested our code, it is possible that there exists rare bugs that only occur under certain edge cases.

SPECIAL INSTRUCTIONS:
The executable file is Main, despite the fact that it is made by "make chess" in the Makefile. Other than that, there are no special instructions to be followed.

TESTING:
All end-to-end tests files, including test.sh, are located in the EndTests_Files directory. (Similarily, files used for unit testing are located in Unittest_Files.) The test sets include:
 - 1: Tests piece capturing, game saving, and forefeiting.
 - 2: Tests a series of invalid moves.
 - 3: Tests castling, both kingside and queenside.
 - 4: Fool's Mate: Tests checkmating in 4 moves.
 - 5: Scholar's Mate: Tests checkmating in 7 moves.
 - 6: Tests stalemating oneself to avoid check.
 - 7: Tests promoting a pawn to a queen.

DISPLAY DESIGN:
 - NOTE: We make no guarentees on the portability of the board display, e.g. in terms of whether the system supports Unicode or what its default fonts and colors are.
 - Unicode is used to display the pieces and the border pattern (specifically U+265A to U+265F for the former and U+2550 to U+256C for the latter).
 - The colors of the borders and the pieces - blue and yellow - reflect the fact that both of us graduated from Franklin High School, New Jersey, which has blue and gold as its school colors.
 - The borders feature patterns that randomly change every time the board is displayed.
 - The borders also feature coordinates (in algerbaic chess notation), in order to assist players with inputting coordinates.
 - Each piece is comprised of four sprites, as opposed to the usual one, for better visibility.
 - The coordinates and pieces are also doubled up due to the fact that this board works with an even number of "pixels" per square.

TRIVIA:
 - This program contains 2,787 lines of code, when including all .h files, all .cpp files, the Makefile, and the testing shell script. This figure also includes comments and spaces.
