#include "Chess.h"
#include "FileIO.h"
#include "Interface.h"
#include "Prompts.h"

int main() {
    int startInput = GameInterface::getStartMenuInput();
    ChessGame chess;
    if (startInput == 1) { // Start the game from the beginning
        chess.setupBoard();
        chess.run();
    } else { // Load the game
        try {
            FileHandler fh(true);
            FileHandler::GameData gameData = fh.readFile();
            chess.loadBoard(gameData.first, gameData.second);
            chess.run();
        } catch (std::runtime_error& e) {
            Prompts::loadFailure();
        }
    }
}
